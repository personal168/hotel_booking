package raviraj.work.springboot.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import raviraj.work.springboot.entity.User;
import raviraj.work.springboot.service.RoomBookingService;
import raviraj.work.springboot.service.UserService;

@WebMvcTest(value = RoomBookingController.class)
public class RoomBookingControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private RoomBookingService roomBookingService;

	@MockBean
	private UserService userService;

	@Test
	public void bookRoomWhenAllInputsAreValidTest() throws Exception {
		ResponseEntity<String> mockResponseString = ResponseEntity.ok("Room Number 1 is BOOKED for user Bin Satyam");
		String mockJsonUser = "{\r\n" + "    \"id\":63,\r\n" + "    \"bonusPoints\":2500,\r\n"
				+ "    \"firstName\": \"Bin\",\r\n" + "    \"lastName\": \"Satyam\",\r\n"
				+ "    \"mobileNumber\": \"+91 8879997814\"}";
		
		Mockito.when(userService.isValidUser(Mockito.anyString())).thenReturn(true);
		Mockito.when(userService.isValidBonusPoints(Mockito.anyDouble())).thenReturn(true);
		Mockito.when(roomBookingService.bookRoom(Mockito.anyLong(), Mockito.any(User.class)))
				.thenReturn(mockResponseString);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.put("https://localhost:8080/api/v1/user/room/1")
				.accept(MediaType.APPLICATION_JSON).content(mockJsonUser).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}
	
	@Test
	public void bookRoomThrowsResourceNotFoundExceptionTest() throws Exception {
		String mockJsonUser = "{\r\n" + "    \"id\":63,\r\n" + "    \"bonusPoints\":2500,\r\n"
				+ "    \"firstName\": \"Bin\",\r\n" + "    \"lastName\": \"Satyam\",\r\n"
				+ "    \"mobileNumber\": \"+91 8879997814\"}";
		
		Mockito.when(userService.isValidUser(Mockito.anyString())).thenReturn(false);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.put("https://localhost:8080/api/v1/user/room/1")
				.accept(MediaType.APPLICATION_JSON).content(mockJsonUser).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
	}
	
	@Test
	public void bookRoomThrowsInvalidInputExceptionTest() throws Exception {
		String mockJsonUser = "{\r\n" + "    \"id\":63,\r\n" + "    \"bonusPoints\":2500,\r\n"
				+ "    \"firstName\": \"Bin\",\r\n" + "    \"lastName\": \"Satyam\",\r\n"
				+ "    \"mobileNumber\": \"+91 8879997814\"}";
		
		Mockito.when(userService.isValidUser(Mockito.anyString())).thenReturn(true);
		Mockito.when(userService.isValidBonusPoints(Mockito.anyDouble())).thenReturn(false);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.put("https://localhost:8080/api/v1/user/room/1")
				.accept(MediaType.APPLICATION_JSON).content(mockJsonUser).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
	}
}
