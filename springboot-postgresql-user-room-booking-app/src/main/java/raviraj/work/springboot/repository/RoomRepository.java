package raviraj.work.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import raviraj.work.springboot.entity.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

}
