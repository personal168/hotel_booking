package raviraj.work.springboot.enums;

public enum RoomStatus {
	AVAILABLE("AVAILABLE"),
	NOT_AVAILABLE("NOT_AVAILABLE"),
	BOOKED("BOOKED"), 
	PENDING_FOR_APPROVAL("PENDING_FOR_APPROVAL");

	private String status;

	private RoomStatus(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return this.status;
	}
}
