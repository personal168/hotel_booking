package raviraj.work.springboot.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import raviraj.work.springboot.entity.User;
import raviraj.work.springboot.exception.InvalidInputException;
import raviraj.work.springboot.exception.ResourceNotFoundException;
import raviraj.work.springboot.model.UserModel;
import raviraj.work.springboot.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Transactional
	public ResponseEntity<User> updateUser(Long userId, User userToBeUpdated)
			throws ResourceNotFoundException, InvalidInputException {
		User user = this.userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for userID=" + userId));

		// Check duplicate Mobile Number
		if (!user.getMobileNumber().equals(userToBeUpdated.getMobileNumber())) {
			Optional<User> opUser = this.userRepository.findByMobileNumber(userToBeUpdated.getMobileNumber());
			if (opUser.isPresent()) {
				User userByMobileNumber = opUser.get();
				if (user.getId() != userByMobileNumber.getId()) {
					throw new InvalidInputException("Mobile Number:" + userToBeUpdated.getMobileNumber()
							+ " is associated with another user, please enter unique one");
				}
			}
		}

		// Mobile Number is not associated with any user, hence update the user details
		user.setBonusPoints(userToBeUpdated.getBonusPoints());
		user.setFirstName(userToBeUpdated.getFirstName());
		user.setLastName(userToBeUpdated.getLastName());
		user.setMobileNumber(userToBeUpdated.getMobileNumber());
		user.setRooms(userToBeUpdated.getRooms());
		User savedUser = this.userRepository.save(user);
		return ResponseEntity.status(HttpStatus.OK).body(savedUser);
	}
	
	public UserModel buildUserModel(User user) {
		UserModel model = new UserModel(user.getBonusPoints(), user.getFirstName(), user.getLastName(), user.getMobileNumber());
		model.setId(user.getId());
		return model;
	}
	
	@Transactional(readOnly = true) // to improve the read performance
	public boolean isValidUser(String mobileNumber) {
		return this.userRepository.findByMobileNumber(mobileNumber).isPresent();
	}
	
	public boolean isValidBonusPoints(Double bonusPoints) {
		if(bonusPoints <= 0) {
			return false;
		}
		return true;
	}
}
