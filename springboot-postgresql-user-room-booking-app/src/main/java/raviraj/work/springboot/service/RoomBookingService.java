package raviraj.work.springboot.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import raviraj.work.springboot.entity.Room;
import raviraj.work.springboot.entity.User;
import raviraj.work.springboot.enums.RoomStatus;
import raviraj.work.springboot.repository.RoomRepository;
import raviraj.work.springboot.repository.UserRepository;

@Service
public class RoomBookingService {

	@Autowired
	private RoomRepository roomRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private EntityManager em;

	/**
	 * Books a Room based on user bonus points. This method checks a room status, if
	 * room is not booked by any other user or it's status is PENDING_FOR_APPROVAL
	 * and current user wants to book a same room and he has good bonus points then
	 * this API allows current user to book a room. Otherwise room status changes to
	 * PENDING_FOR_APPROVAL and entry will be created in user_room table for future
	 * booking
	 * 
	 * @param roomId
	 * @param user
	 * @return
	 */
	@Transactional
	public ResponseEntity<String> bookRoom(Long roomId, User user) {
		Optional<Room> roomToBeBooked = roomRepository.findById(roomId);
		if (roomToBeBooked.isPresent()) {
			Room room = roomToBeBooked.get();
			if (isRoomAvailable(room.getStatus()) /* && room.getPrice() <= user.getBonusPoints() */) {
				if (room.getPrice() <= user.getBonusPoints()) {
					room.setStatus(RoomStatus.BOOKED.getStatus());
					user.setBonusPoints(user.getBonusPoints() - room.getPrice());
					
					// To avoid multiple DB calls and instead of creating another entity and repository 
					// for user_room table, using following native query
					String queryStr = "DELETE FROM user_room ur WHERE ur.room_id = ?";
					Query q = em.createNativeQuery(queryStr);
					q.setParameter(1, room.getId());
					q.executeUpdate();
				} else {
					room.setStatus(RoomStatus.PENDING_FOR_APPROVAL.getStatus());
				}
				user.addRoom(room);
				userRepository.save(user);
			}
			return ResponseEntity.ok().body("Room Number " + roomId + " is " + room.getStatus() + " for user "
					+ user.getFirstName() + " " + user.getLastName());
		}
		return ResponseEntity.ok().body("Room Number " + roomId + " is " + RoomStatus.NOT_AVAILABLE.getStatus());
	}

	public boolean isRoomAvailable(String status) {
		if (StringUtils.isEmpty(status)) {
			return true;
		}
		if (RoomStatus.AVAILABLE.getStatus().equalsIgnoreCase(status)) {
			return true;
		}
		if (RoomStatus.PENDING_FOR_APPROVAL.getStatus().equalsIgnoreCase(status)) {
			return true;
		}
		return false;
	}

	/**
	 * If user has multiple booking and those are in PENDING_FOR_APPROVAL state then
	 * this API books multiple rooms based on user bonus points. If user has valid
	 * bonus points then all rooms will be booked otherwise only few will be booked.
	 * 
	 * @param user
	 * @return
	 */
	@Transactional
	public ResponseEntity<String> bookRooms(User user) {
		Set<Room> roomsToBeBooked = new HashSet<Room>();
		List<Long> pendingRoomIdsForCurrentUser = new ArrayList<Long>();

		user.getRooms().forEach(room -> {
			if (RoomStatus.PENDING_FOR_APPROVAL.getStatus().equalsIgnoreCase(room.getStatus())) {
				pendingRoomIdsForCurrentUser.add(room.getId());
			}
		});

		// To check whether these rooms are still Pending for Approval
		List<Room> allPendingRooms = roomRepository.findAllById(pendingRoomIdsForCurrentUser);
		allPendingRooms.forEach(room -> {
			if (RoomStatus.PENDING_FOR_APPROVAL.getStatus().equalsIgnoreCase(room.getStatus())
					&& user.getBonusPoints() >= room.getPrice()) {
				room.setStatus(RoomStatus.BOOKED.getStatus());
				roomsToBeBooked.add(room);
				user.setBonusPoints(user.getBonusPoints() - room.getPrice());
			}
		});
		if (roomsToBeBooked.size() > 0) {
			// To ensure all Pending rooms are to be persisted for future booking
			user.getRooms().addAll(roomsToBeBooked);
			user.setRooms(roomsToBeBooked);
			userRepository.save(user);
			return ResponseEntity.ok().body("Room with Room Number (" + roomsToBeBooked + ") booked for user "
					+ user.getFirstName() + " " + user.getLastName());
		}

		return ResponseEntity.ok().body("Room is not available for your bonus points");
	}
}
