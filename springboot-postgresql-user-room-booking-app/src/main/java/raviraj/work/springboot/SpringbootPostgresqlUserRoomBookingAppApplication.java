package raviraj.work.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootPostgresqlUserRoomBookingAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootPostgresqlUserRoomBookingAppApplication.class, args);
	}

}
