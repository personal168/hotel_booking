package raviraj.work.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import raviraj.work.springboot.entity.User;
import raviraj.work.springboot.exception.InvalidInputException;
import raviraj.work.springboot.exception.ResourceNotFoundException;
import raviraj.work.springboot.service.RoomBookingService;
import raviraj.work.springboot.service.UserService;

@RestController
@RequestMapping("api/v1")
public class RoomBookingController {

	@Autowired
	private RoomBookingService roomBookingService;

	@Autowired
	private UserService userService;

	@PutMapping("user/room/{id}")
	public ResponseEntity<String> bookRoom(@PathVariable(value = "id") Long roomId, @Validated @RequestBody User user)
			throws InvalidInputException, ResourceNotFoundException {
		if (!userService.isValidUser(user.getMobileNumber())) {
			throw new ResourceNotFoundException(
					"User not found for userID=" + user.getId());
		}
		if (!userService.isValidBonusPoints(user.getBonusPoints())) {
			throw new InvalidInputException("Bonus Points should be greater than zero.");
		}
		return roomBookingService.bookRoom(roomId, user);
	}
}
