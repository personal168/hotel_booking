package raviraj.work.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import raviraj.work.springboot.entity.User;
import raviraj.work.springboot.exception.InvalidInputException;
import raviraj.work.springboot.exception.ResourceNotFoundException;
import raviraj.work.springboot.model.UserModel;
import raviraj.work.springboot.service.RoomBookingService;
import raviraj.work.springboot.service.UserService;

@RestController
@RequestMapping("api/v1")
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private RoomBookingService roomService;

	// update user
	@PutMapping("/user/{id}")
	public ResponseEntity<UserModel> updateUser(@PathVariable(value = "id") Long userId,
			@Validated @RequestBody User userDetails) throws ResourceNotFoundException, InvalidInputException {
		ResponseEntity<User> response = userService.updateUser(userId, userDetails);
		roomService.bookRooms(response.getBody());
		return ResponseEntity.ok().body(userService.buildUserModel(response.getBody()));
	}
}
