package raviraj.work.springboot.model;

import java.util.HashSet;
import java.util.Set;

public class RoomModel {

	private Long id;
	private Double price;
	private String type;
	private Integer capacity;
	private String status;
	private String shareable;
	private Set<UserModel> users;

	public RoomModel() {
//		super();
	}

	public RoomModel(Double price, String type, Integer capacity, String status, String isShareable) {
		super();
		this.price = price;
		this.type = type;
		this.capacity = capacity;
		this.status = status;
		this.shareable = isShareable;
	}

	public Set<UserModel> getUsers() {
		if(users == null) {
			users = new HashSet<UserModel>();
		}
		return users;
	}

	public void setUsers(Set<UserModel> users) {
		this.users = users;
	}

	public boolean addUser(UserModel user) {
		return getUsers().add(user);
	}
	
	public boolean removeUser(UserModel user) {
		return users.remove(user);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String isShareable() {
		return shareable;
	}

	public void setShareable(String isShareable) {
		this.shareable = isShareable;
	}

}
