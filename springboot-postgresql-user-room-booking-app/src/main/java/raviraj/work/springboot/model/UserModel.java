package raviraj.work.springboot.model;

import java.util.HashSet;
import java.util.Set;

public class UserModel {

	private Long id;
	private Double bonusPoints;
	private String firstName;
	private String lastName;
	private String mobileNumber;
	private Set<RoomModel> rooms;

	public UserModel() {
		super();
	}

	public UserModel(Double bonusPoints, String firstName, String lastName, String mobileNumber) {
		super();
		this.bonusPoints = bonusPoints;
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNumber = mobileNumber;
	}

	public Set<RoomModel> getRooms() {
		if (rooms == null) {
			rooms = new HashSet<RoomModel>();
		}
		return rooms;
	}

	public void setRooms(Set<RoomModel> rooms) {
		this.rooms = rooms;
	}

	public boolean addRoom(RoomModel room) {
		return getRooms().add(room);
	}

	public boolean removeRoom(RoomModel room) {
		return getRooms().remove(room);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Double getBonusPoints() {
		return bonusPoints;
	}

	public void setBonusPoints(Double bonusPoints) {
		this.bonusPoints = bonusPoints;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

}
