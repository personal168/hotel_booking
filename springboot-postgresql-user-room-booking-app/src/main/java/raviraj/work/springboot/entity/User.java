package raviraj.work.springboot.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "Users")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "bonus_points", nullable = false)
	private Double bonusPoints;

	@Column(name = "first_name", nullable = false)
	private String firstName;

	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Column(name = "mobile_number", nullable = false, unique = true)
	private String mobileNumber;

	@ManyToMany(targetEntity = Room.class, fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.DETACH,
			CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE })
	@JoinTable(name = "user_room", joinColumns = {
			@JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "room_id", referencedColumnName = "id") })
//	@JsonBackReference
	private Set<Room> rooms; // Lazily initialized in getRooms()
	
	public User() {
//		super();
	}

	public User(Double bonusPoints, String firstName, String lastName, String mobileNumber) {
		super();
		this.bonusPoints = bonusPoints;
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNumber = mobileNumber;
	}

	public Set<Room> getRooms() {
		if (rooms == null) {
			rooms = new HashSet<Room>();
		}
		return rooms;
	}

	public void setRooms(Set<Room> rooms) {
		this.rooms = rooms;
	}

	public boolean addRoom(Room room) {
		if (getRooms().contains(room)) {
			return false;
		}
		return rooms.add(room);
	}

	public boolean addRoom(Set<Room> roomToBeAdded) {
		if (getRooms().size() < 1) {
			return rooms.addAll(roomToBeAdded);
		}
		boolean isRemoved = rooms.removeIf(room -> !roomToBeAdded.contains(room));
		if (isRemoved) {
			return rooms.addAll(roomToBeAdded);
		}
		return false;
	}

	public boolean removeRoom(Room room) {
		return rooms.remove(room);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getBonusPoints() {
		return bonusPoints;
	}

	public void setBonusPoints(Double bonusPoints) {
		this.bonusPoints = bonusPoints;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

}
