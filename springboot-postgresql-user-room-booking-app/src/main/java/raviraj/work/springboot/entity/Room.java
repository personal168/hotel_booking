package raviraj.work.springboot.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "Rooms")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Room {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "price", nullable = false)
	private Double price;

	@Column(name = "type", nullable = false)
	private String type;

	@Column(name = "capacity", nullable = false)
	private Integer capacity;

	@Column(name = "status", nullable = false)
	private String status;

	@Column(name = "shareable", nullable = false)
	private String shareable;

	@ManyToMany(targetEntity = User.class, fetch = FetchType.LAZY, mappedBy = "rooms", cascade = { CascadeType.PERSIST,
			CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE })
//	@JsonManagedReference
	private Set<User> users = new HashSet<User>();

	public Room() {

	}

	public Room(Double price, String type, Integer capacity, String status, String isShareable) {
		super();
		this.price = price;
		this.type = type;
		this.capacity = capacity;
		this.status = status;
		this.shareable = isShareable;
	}

	public Set<User> getUsers() {
		if (users == null) {
			users = new HashSet<User>();
		}
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public boolean addUser(User user) {
		return users.add(user);
	}

	public boolean removeUser(User user) {
		return users.remove(user);
	}

	public Long getId() {
		return id;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getShareable() {
		return shareable;
	}

	public void setShareable(String isShareable) {
		this.shareable = isShareable;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
